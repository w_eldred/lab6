/*
 *Ward Eldred
 *CPSC 1021, 004, F20
 *weldred@clemson.edu
 *Elliot McMillan & Matt Franchi
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
using namespace std;
typedef struct Employee{
    string lastName;
    string firstName;
    int birthYear;
    double hourlyWage;
} employee;

//declares functions
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}
int main(int argc,char const *argv[]) {

    int place = 0;

    // IMPLEMENT as instructed below
    /* This is to seed the random generator */
    srand(unsigned (time(0)));

    /*Create an array of 10 employees and fill information from standard input with prompt messages*/
    /*After the array is created and initialized we call random_shuffle() see the*/
    /*notes to determine the parameters to pass in.*/

    employee faculty[10];

    //iterates through 10 places
    while(place < 10){

        //user prompted to enter last name
        cout << "Enter the last name of the employee: " << endl;
        cin >> faculty[place].lastName;
        cout << endl;

        //user prompted to enter first name
        cout << "Enter the first name of the employee: " << endl;
        cin >> faculty[place].firstName;
        cout << endl;

        //user prompted to enter birth year
        cout << "Enter the year the employee was born: " << endl;
        cin >> faculty[place].birthYear;
        cout << endl;

        //user prompted to enter hourly wage
        cout << "Enter the hourly wage of the employee (please use a decimal point): " << endl;
        cin >> faculty[place].hourlyWage;
        cout << endl;

        place++;
    }

    //shuffles the array of faculty names and data
    random_shuffle(&faculty[0], &faculty[10], &myrandom);

    /*Build a smaller array of 5 employees from the first five cards of the array created
     * *above*/
    employee smallGroupFaculty[5];
    for (int i = 0 ; i < 4 ; i++){
        smallGroupFaculty[i] = faculty[i];
    }


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
    sort(&smallGroupFaculty[0], &smallGroupFaculty[5],name_order);

    /*Now print the array below */
    for(employee temp: smallGroupFaculty){
        cout << "\t" << setw(5) << temp.lastName << ", " << temp.firstName<< endl;
        cout << "\t" << setw(5) << temp.birthYear << endl;
        cout << "\t" << setw(5) << showpoint << fixed << setprecision(2) << temp.hourlyWage << endl;
    }

    return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {

    //checks last names to see if one goes before another
    if(&lhs.lastName < &rhs.lastName){
        return true;
    }
    else{
        return false;
    }
}

