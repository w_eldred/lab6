/*
 *Ward Eldred
 *CPSC 1021, 004, F20
 *weldred@clemson.edu
 *Elliot McMillan & Matt Franchi
*/

#include <stdio.h>
#include "SimpleLL.h"

int main() {

    //sets local variable
    int userInput = 0;
    int userAddData;

    //sets the first 4 nodes of the linkedList
    node_t first, second, third, fourth;
    node_t *head;

    //sets data for each of the four nodes
    first.data = 2;
    second.data = 5;
    third.data = 3;
    fourth.data = 9;

    //sets location in the linked list for each
    head = &first;
    first.next = &second;
    second.next = &third;
    third.next = &fourth;
    fourth.next = NULL;

    //loops through options while user is editing the linked list
    while(userInput != -1){

        //prints a list of options
        printf("Enter 1 to append a node\n");
        printf("Enter 2 to add a node to the front of the linked list\n");
        printf("Enter 3 to delete the list\n");
        printf("Enter -1 to leave\n");
        scanf("%d", &userInput);

        switch(userInput){

            case 1:

                //asks for an input
                printf("Enter a number, which will be stored as the data for one of the nodes:");
                scanf("%d", &userAddData);
                printf("\n");

                //calls the appropriate function from SimpleLL.c and prints the list
                append(userAddData);
                printList();
                break;

            case 2:

                //prompts user for an integer
                printf("Enter a number, which will be stored as the data for one of the nodes:");
                scanf("%d", &userAddData);
                printf("\n");

                //inputs user's data into
                addFront(userAddData);
                printList();
                break;

            case 3:

                //double checks with user to make sure they want to delete the linked list
                printf("Type 1 to CONFIRM that you would like to delete this list.\n");
                printf("Type 0 to KEEP this list and go back.\n");
                printf("\n");
                scanf("%d", &userAddData);
                printf("\n");

                //checks user input
                if(userAddData == 1){
                    printf("After all this work, the Linked List will be deleted in 3,2,1 ...");
                    printf("\n");
                    deleteList();
                    printList();

                    break;
                }
                else{
                    break;
                }

            //checks to make sure that the user's input is valid
            default:
                printf("Please enter a number 1 through 3 or enter -1 to leave.\n");
                printf("\n");
                break;
        }

    }

    return 0;

}
